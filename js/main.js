//This thing will hide address bar
if (navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('Android') != -1) {
	//console.log("device");
	window.addEventListener("load", function () {
		setTimeout(function () {
			//console.log();
			if (window.location.hash.indexOf('#') == -1) {
				window.scrollTo(0, 1);
			}
		}, 0);
	}, false);
}

$(function() {
	initPikame();
});


function initPikame() {
	$('#gallery_01 a').first().addClass("active");

	$('#gallery_01 a').on('click', function(e) {
		e.preventDefault();

		$(this).siblings('.active').removeClass("active");
		$(this).addClass("active");

		var newImg		= new Image();
		var imageSrc	= $(this).attr('data-image');
		var targetImg	= $("#pikame");

		newImg.onload = function() {
			targetImg.attr("src", imageSrc);
		}
		newImg.src = imageSrc;
	});

	//pass the images to Fancybox
	$(".pika-stage").on("click", function(e) {
		galleryList = [];
		$('#gallery_01 a').each(function() {
			element = {href: $(this).attr('data-image'), title: $(this).find('img').attr('title')};
			if ($(this).hasClass('active')) {
				galleryList.unshift(element);
			} else {
				galleryList.push(element);
			}
		});
		$.fancybox(galleryList);
		return false;
	});

	$(".pika-big-stage").on("click", function(e) {
		galleryList = [];
		$('#gallery_01 a').each(function() {
			element = {href: $(this).attr('data-big-image'), title: $(this).find('img').attr('title')};
			if ($(this).hasClass('active')) {
				galleryList.unshift(element);
			} else {
				galleryList.push(element);
			}
		});
		$.fancybox(galleryList);
		return false;
	});

	if ($('a.single_image').length !== 0) {
		$("a.single_image").fancybox();
	}
}