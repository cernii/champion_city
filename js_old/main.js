//This thing will hide address bar
if (navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('Android') != -1) {
	//console.log("device");
	window.addEventListener("load", function () {
		setTimeout(function () {
			//console.log();
			if (window.location.hash.indexOf('#') == -1) {
				window.scrollTo(0, 1);
			}
		}, 0);
	}, false);
}

$(function() {
	initPikame();
});


function initPikame() {
	$('.pika-gallery').each(function () {
		$(this).find('a').first().addClass("active");

		$(this).find('a').click(function(e) {
			e.preventDefault();

			$(this).siblings('.active').removeClass("active");
			$(this).addClass("active");

			var newImg = new Image();
			var imageSrc  = $(this).attr('data-image');
			var targetImg = $(this).closest(".pikame-view").find('img:first');

			newImg.onload = function() {
				targetImg.attr("src", imageSrc);
			}
			newImg.src = imageSrc;

		});
	});

	//pass the images to Fancybox
	$(".pika-stage").bind("click", function(e) {
		galleryList = [];
		$(this).parent().find('.pika-gallery a').each(function() {
			element = {href: $(this).attr('data-image'), title: $(this).find('img').attr('title')};
			if ($(this).hasClass('active')) {
				galleryList.unshift(element);
			} else {
				galleryList.push(element);
			}
		});
		$.fancybox(galleryList);
		return false;
	});

	$(".pika-big-stage").bind("click", function(e) {
		galleryList = [];
		$(this).parent().find('.pika-gallery a').each(function() {
			element = {href: $(this).attr('data-big-image'), title: $(this).find('img').attr('title')};
			if ($(this).hasClass('active')) {
				galleryList.unshift(element);
			} else {
				galleryList.push(element);
			}
		});
		$.fancybox(galleryList);
		return false;
	});

	$("a.single_image").fancybox();
}