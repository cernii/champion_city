var preset = {
	background: {width: 3840, height: 2160},
	shadows: [
		{name: ".shd1",	left: 11.3,	top: 1.4,	width: 21,		height: 59,		left_sign: "-",	top_sign: "-"},
		{name: ".shd2",	left: 1,	top: 12.5,	width: 16.1,	height: 48.14,	left_sign: "+",	top_sign: "-"},
		{name: ".shd3",	left: 11.82,	top: 2.72,	width: 26.82,	height: 57.31,	left_sign: "+",	top_sign: "-"}
	],
	bubbles: [
		{name: ".bb1", left: "35", top: "5",   position: "1", left_sign: "+", top_sign: "-"}
	]
};

$(function() {
	$(window).resize(function() {
		setPositions();
	}).resize();

	$(document).on('touchstart', function(event) {
		$target = $(event.target);
		$('.shd').css('opacity', 0.01);
		$('.shd').children().css('display', 'none');

		if ($target.closest('.shd').length == 0) {
			return;
		}

		$target.closest('.shd').css('opacity', 1);
		$target.closest('.shd').find('.big-bubble').css('display', 'inline-block');
	});
});

function setPositions()
{
	originalWindowWidth		= $(window).width();
	originalWindowHeight	= $(window).height();
	windowWidth				= originalWindowWidth;
	windowHeight			= originalWindowHeight;

	if (originalWindowWidth / originalWindowHeight > preset.background.width / preset.background.height) {
		windowHeight = originalWindowWidth * preset.background.height / preset.background.width;
	}
	else {
		windowWidth = originalWindowHeight * preset.background.width / preset.background.height;
	}

	$('.back-bg')
		.width(originalWindowWidth)
		.height(originalWindowHeight);
	$('.back-bg img')
		.width(windowWidth)
		.height(windowHeight)
		.css("margin-left", "-" + Math.round(windowWidth / 2, 1) + "px")
		.css("margin-top", "-" + Math.round(windowHeight / 2, 1) + "px");

	// shadows positioning and resizing
	$.each(preset.shadows, function() {
		element_left	= Math.round(windowWidth * parseFloat(this.left) / 100, 1);
		element_top		= Math.round(windowHeight * parseFloat(this.top) / 100, 1);
		element_width	= Math.round(windowWidth * parseFloat(this.width) / 100, 1);
		element_height	= Math.round(windowHeight * parseFloat(this.height) / 100, 1);

		$(this.name).css('left', 0);
		$(this.name).css('top', 0);

		$(this.name).width(element_width);
		$(this.name).height(element_height);

		$(this.name).position({
			my: "center center",
			at: "center" + this.left_sign + element_left + " center" + this.top_sign + element_top,
			of: window,
			collision: "none"
		});
	});

	// bubbles positioning with calcutaling their arrows offsets
	$.each(preset.bubbles, function() {
		element_left	= Math.round(windowWidth * parseFloat(this.left) / 100, 1);
		element_top		= Math.round(windowHeight * parseFloat(this.top) / 100, 1);

		$(this.name).css('left', 0);
		$(this.name).css('top', 0);

		$(this.name).position({
			my: "center center",
			at: "center" + this.left_sign + element_left + " center" + this.top_sign + element_top,
			of: window,
			collision: "none"
		});
	});
}